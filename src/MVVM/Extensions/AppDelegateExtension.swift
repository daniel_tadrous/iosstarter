//
//  AppDelegateExtension.swift
//  MVVM
//
//  Created by Daniel Tadrous on 9/26/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation
import Swinject
import SwinjectAutoregistration
import SwinjectStoryboard

extension AppDelegate {
    
    
    func setupDependencies() {
        // services
        self.container.autoregister(IService.self, initializer: Service.init)
        
        //viewModels
        self.container.autoregister(MainViewModel.self, initializer: MainViewModel.init)
        self.container.autoregister(SecondViewModel.self, initializer: SecondViewModel.init)
        
        //viewControllers
       self.container.storyboardInitCompleted(MainViewController.self) { r, c
            in c.viewModel = r.resolve(MainViewModel.self)
        }
        self.container.storyboardInitCompleted(SecondViewController.self) { r, c
            in c.viewModel = r.resolve(SecondViewModel.self)
        }
    }
    
}
