//
//  StoryboardLodableExtension.swift
//  MVVM
//
//  Created by Daniel Tadrous on 9/27/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation
import UIKit

protocol MainViewControllerStoryboardLoadable: StoryboardLodable {
}

extension MainViewControllerStoryboardLoadable where Self: UIViewController {
    static var storyboardName: String {
        return "Main"
    }
}

protocol SecondViewControllerStoryboardLoadable: StoryboardLodable {
}

extension SecondViewControllerStoryboardLoadable where Self: UIViewController {
    static var storyboardName: String {
        return "Second"
    }
}
