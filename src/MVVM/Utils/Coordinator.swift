//
//  Coordinator.swift
//  MVVM
//
//  Created by Daniel Tadrous on 9/27/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation
import UIKit
import Swinject
import SwinjectAutoregistration
import RxSwift

enum ScreensEnum:String{
    case MainViewController,SecondViewController
}


class Coordinator {
    private let window: UIWindow
    let container: Container
    let navigationController : UINavigationController
    static var shared: Coordinator!
    private var currentScreen: ScreensEnum!
    
    init(window: UIWindow, container: Container) {
        self.window = window
        self.container = container
        self.navigationController = UINavigationController()
        self.window.rootViewController = self.navigationController
        
        Coordinator.shared = self
    }
    func hideNavigationBar(_ hide:Bool = false){
        self.navigationController.navigationBar.isHidden = hide
    }
    func load(screenEnum: ScreensEnum, _ animated: Bool = true){
        currentScreen = screenEnum
        switch screenEnum {
            case .MainViewController:
                let vc = container.resolveViewController(MainViewController.self)
                hideNavigationBar(true)
                navigationController.pushViewController(vc, animated: animated)
        case .SecondViewController:
            let vc = container.resolveViewController(SecondViewController.self)
            hideNavigationBar(false)
            navigationController.pushViewController(vc, animated: animated)
            
        }
    }
    
}
