//
//  MainViewController.swift
//  MVVM
//
//  Created by Daniel Tadrous on 9/26/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import UIKit
import RxSwift

class MainViewController: UIViewController, MainViewControllerStoryboardLoadable {

    var viewModel: MainViewModel!
    private let disposeBag = DisposeBag()
    private var cats: [Category] = []{
        didSet{
           print(cats)
        }
    }
    @IBAction func clickHandler(_ sender: UIButton) {
        Coordinator.shared.load(screenEnum: .SecondViewController)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.cats.asObservable().subscribe(onNext: { [unowned self] (cats) in
            if let cats = cats{
                self.cats = cats
            }
        }).disposed(by: disposeBag)
    }


}

