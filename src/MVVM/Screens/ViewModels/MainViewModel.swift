//
//  MainViewModel.swift
//  MVVM
//
//  Created by Daniel Tadrous on 9/27/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation
import RxSwift

class MainViewModel{
    
    private var service: IService
    private let disposeBag = DisposeBag()
    let cats = Variable<[Category]?>(nil)
    init(service: IService) {
        self.service = service
        self.getCategories()
    }
    private func getCategories(){
        self.service.getCategories().subscribe(onNext: { [unowned self] (cats) in
            if let cats = cats{
                self.cats.value = cats
                self.addCategory()
            }
        }).disposed(by: disposeBag)
    }
    func addCategory(){
        self.service.addCategory(category: Category(name: "new", icon: "newicon", color: "newcolor")).subscribe(onNext: { (cat) in
            print(cat)
        }).disposed(by: disposeBag)
    }
}
