//
//  MainService.swift
//  MVVM
//
//  Created by Daniel Tadrous on 9/26/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation
import RxSwift
import AlamofireObjectMapper
import Alamofire

protocol IService{
    func getCategories()->Observable<[Category]?>
    func addCategory(category:Category) -> Observable<Category?>
}

class Service:BaseService, IService{
    func getCategories() -> Observable<[Category]?> {
        let result = Variable<[Category]?>(nil)
        let url = "\(baseUrl)/categories"
        Alamofire.request(url).responseArray { (response: DataResponse<[Category]>) in
            result.value = response.result.value ?? []
        }
        return result.asObservable()
    }
    func addCategory(category:Category) -> Observable<Category?> {
        let result = Variable<Category?>(nil)
        let url = "\(baseUrl)/categories"
        Alamofire.request(url, method: .post, parameters: category.toJSON(), encoding: URLEncoding.default, headers: nil).responseObject(completionHandler: { (response:DataResponse<Category>) in
          result.value = response.result.value
        })
        return result.asObservable()
    }
    
}
