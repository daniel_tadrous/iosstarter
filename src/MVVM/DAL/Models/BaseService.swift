//
//  BaseService.swift
//  MVVM
//
//  Created by Daniel Tadrous on 9/27/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation
class BaseService{
    
    var baseUrl: String{
        get{
            return (Bundle.main.object(forInfoDictionaryKey: "base_url") as? String)!
        }
    }
}
