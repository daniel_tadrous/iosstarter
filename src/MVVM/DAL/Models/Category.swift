//
//  Category.swift
//  MVVM
//
//  Created by Daniel Tadrous on 9/27/18.
//  Copyright © 2018 Daniel Tadrous. All rights reserved.
//

import Foundation
import ObjectMapper

class Category: Mappable{
    
    var id: Int?
    var name: String?
    var icon: String?
    var color: String?
    init(name: String?, icon: String?,color: String?){
        self.name = name
        self.icon = icon
        self.color = color
    }
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        icon <- map["icon"]
        color <- map["color"]
    }
}
